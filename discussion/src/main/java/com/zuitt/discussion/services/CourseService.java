package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {
    void createCourse(String stringToken, Course course);
    Iterable<Course> getCourses();

    ResponseEntity deleteCourse(Long id, String stringToken);

    ResponseEntity updateCourse(Long id, String stringToken, Course course);

    Iterable<Course> getMyCourses(String stringToken);

}

